package fr.wedoogift.backend.exceptions;

public class BalanceException extends RuntimeException {
    public BalanceException(String message) {
        super(message);
    }
}
