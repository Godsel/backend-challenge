package fr.wedoogift.backend.exceptions;

public class UnexpectedDepositType
        extends RuntimeException {
    public UnexpectedDepositType(String message) {
        super(message);
    }
}
