package fr.wedoogift.backend.exceptions;

public class UnexistantIdException
        extends RuntimeException {
    public UnexistantIdException(String message) {
        super(message);
    }
}
