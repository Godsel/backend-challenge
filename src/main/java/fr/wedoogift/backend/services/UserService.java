package fr.wedoogift.backend.services;

import fr.wedoogift.backend.exceptions.UnexistantIdException;
import fr.wedoogift.backend.models.dto.BalanceDto;
import fr.wedoogift.backend.models.dto.DepositCreationDto;
import fr.wedoogift.backend.models.dto.DepositDto;
import fr.wedoogift.backend.models.entities.Company;
import fr.wedoogift.backend.models.entities.User;
import fr.wedoogift.backend.models.entities.deposit.Deposit;
import fr.wedoogift.backend.repositories.CompanyRepository;
import fr.wedoogift.backend.repositories.DepositRepository;
import fr.wedoogift.backend.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;


@Service
public class UserService
        implements BasedUserServiceImpl {
    private final CompanyRepository companyRepository;
    private final DepositRepository depositRepository;
    private final UserRepository userRepository;

    public UserService(CompanyRepository companyRepository,
                       DepositRepository depositRepository,
                       UserRepository userRepository) {
        this.companyRepository = companyRepository;
        this.depositRepository = depositRepository;
        this.userRepository = userRepository;
    }

    public List<DepositDto> postDeposit(Long userId, DepositCreationDto depositCreationDto)
            throws
            UnexistantIdException {
        Optional<User> userAccount = userRepository.findById(userId);
        Optional<Company> company = companyRepository.findById(depositCreationDto.company()
                                                                                 .id());

        if (userAccount.isPresent() && company.isPresent()) {
            return addDeposits(userAccount.get(),
                               company.get(),
                               depositCreationDto.deposits()).stream()
                                                             .map(deposit -> new DepositDto(deposit.getId(),
                                                                                            deposit.getType(),
                                                                                            deposit.getAmount(),
                                                                                            deposit.getDate(),
                                                                                            deposit.getExpiredDate()))
                                                             .toList();
        }
        throw new UnexistantIdException("Required Objects do not exist in database");
    }

    public List<BalanceDto> getBalancesByUserId(Long userId)
            throws
            UnexistantIdException {
        Optional<User> userAccount = userRepository.findById(userId);
        if (userAccount.isPresent()) {
            return userAccount.get()
                              .getBalanceByDepositType()
                              .entrySet()
                              .stream()
                              .map(entry -> new BalanceDto(entry.getValue(),
                                                           entry.getKey()))
                              .sorted(Comparator.comparing(BalanceDto::depositType))
                              .toList();
        }
        throw new UnexistantIdException("Required Objects do not exist in database");
    }

    private List<Deposit> addDeposits(User user, Company company,
                                      List<DepositDto> depositList) {
        Integer size = user.getDepositList()
                           .size();
        for (DepositDto depositDto : depositList) {
            company.provideDeposit(user, depositDto.amount(), depositDto.type());
        }

        companyRepository.save(company);

        Iterable<Deposit> createdDeposits = depositRepository.saveAll(user.getDepositList()
                                                                          .stream()
                                                                          .skip(size)
                                                                          .toList());
        return (List<Deposit>) createdDeposits;
    }


}
