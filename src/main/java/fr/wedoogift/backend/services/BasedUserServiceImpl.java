package fr.wedoogift.backend.services;

import fr.wedoogift.backend.exceptions.UnexistantIdException;
import fr.wedoogift.backend.models.dto.BalanceDto;
import fr.wedoogift.backend.models.dto.DepositCreationDto;
import fr.wedoogift.backend.models.dto.DepositDto;

import java.util.List;

public interface BasedUserServiceImpl {
    List<DepositDto> postDeposit(Long userId, DepositCreationDto depositCreationDto)
            throws
            UnexistantIdException;
    List<BalanceDto> getBalancesByUserId(Long userId)
            throws
            UnexistantIdException;
}
