package fr.wedoogift.backend.models;

import fr.wedoogift.backend.exceptions.UnexpectedDepositType;
import fr.wedoogift.backend.models.entities.Company;
import fr.wedoogift.backend.models.entities.deposit.Deposit;
import fr.wedoogift.backend.models.entities.deposit.Gift;
import fr.wedoogift.backend.models.entities.deposit.Meal;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class DepositFactory {
    public Deposit createDeposit(Integer amount,
                                 Company company,
                                 String depositType) {
        Deposit deposit;
        deposit = switch (depositType) {
            case "gift" -> new Gift(company, amount, LocalDate.now(), null);
            case "meal" -> new Meal(company, amount, LocalDate.now(), null);
            default -> throw new UnexpectedDepositType("This deposit type is not supported.");
        };

        deposit.calculateExpiredDate();
        return deposit;
    }
}
