package fr.wedoogift.backend.models.dto;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

public record DepositDto(Long id,
                         @NotEmpty String type,
                         @NotEmpty Integer amount,
                         LocalDateTime creationDate,
                         LocalDate expiredDate)
        implements Serializable {
}
