package fr.wedoogift.backend.models.dto;

import java.io.Serializable;
public record BalanceDto(Integer amount,
                         String depositType)
        implements Serializable {
}
