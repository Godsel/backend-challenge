package fr.wedoogift.backend.models.dto;

import javax.validation.Valid;
import java.io.Serializable;
import java.util.List;

public record DepositCreationDto(@Valid CompanyDto company,
                                 List<DepositDto> deposits)
        implements Serializable {
}
