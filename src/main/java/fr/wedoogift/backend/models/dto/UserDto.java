package fr.wedoogift.backend.models.dto;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

public record UserDto(Long id,
                      @NotEmpty String name)
        implements Serializable {
}
