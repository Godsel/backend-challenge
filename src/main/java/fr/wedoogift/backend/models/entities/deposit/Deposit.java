package fr.wedoogift.backend.models.entities.deposit;

import fr.wedoogift.backend.models.entities.Company;
import fr.wedoogift.backend.models.entities.User;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "deposits")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type")
public abstract class Deposit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "company_id")
    private Company issuerCompany;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;
    @Column(insertable = false,
            updatable = false)
    private String type;
    @Column
    private Integer amount;
    @Column
    private LocalDateTime date;
    @Column
    private LocalDate expiredDate;

    protected Deposit() {
    }

    protected Deposit(Company issuerCompany, Integer amount, LocalDate date,
                      LocalDate expiredDate, String type) {
        this.issuerCompany = issuerCompany;
        this.amount = amount;
        this.date = date.atStartOfDay();
        this.expiredDate = expiredDate;
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Company getIssuerCompany() {
        return issuerCompany;
    }

    public void setIssuerCompany(Company company) {
        this.issuerCompany = company;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public LocalDate getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(LocalDate expiredDate) {
        this.expiredDate = expiredDate;
    }

    public Boolean isValid() {
        return LocalDate.now()
                        .isBefore(expiredDate);
    }

    public void calculateExpiredDate() {
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
