package fr.wedoogift.backend.models.entities;

import fr.wedoogift.backend.exceptions.BalanceException;
import fr.wedoogift.backend.models.AccountProviderInterface;
import fr.wedoogift.backend.models.DepositFactory;
import fr.wedoogift.backend.models.entities.deposit.Deposit;

import javax.persistence.*;

@Entity
@Table(name = "companies")
public class Company
        implements AccountProviderInterface {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String name;
    @Column
    private Integer balance;

    public Company() {
    }

    public Company(String name, Integer balance) {
        this.name = name;
        this.balance = balance;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    @Override
    public void provideDeposit(User user, Integer amount,
                               String depositType)
            throws
            BalanceException {
        checkViabilityOfOperation(amount);
        DepositFactory depositFactory = new DepositFactory();
        Deposit deposit = depositFactory.createDeposit(amount, this, depositType);
        user.deposit(deposit);
        this.balance = balance - amount;
    }

    private void checkViabilityOfOperation(Integer amount) {
        if (balance < amount) {
            throw new BalanceException("Company's balance is too low to make this deposit");
        }
    }
}
