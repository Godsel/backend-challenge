package fr.wedoogift.backend.models.entities.deposit;

import fr.wedoogift.backend.models.entities.Company;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.time.LocalDate;

@Entity
@DiscriminatorValue("meal")
public class Meal
        extends Deposit {

    public Meal() {
    }

    public Meal(Company issuerCompany, Integer amount,
                LocalDate date,
                LocalDate expiredDate) {
        super(issuerCompany, amount, date, expiredDate, "meal");
    }

    @Override
    public void calculateExpiredDate() {
        super.setExpiredDate(super.getDate()
                                  .toLocalDate()
                                  .plusYears(1)
                                  .withMonth(3)
                                  .withDayOfMonth(1));
    }


}
