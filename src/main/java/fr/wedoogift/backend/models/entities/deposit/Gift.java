package fr.wedoogift.backend.models.entities.deposit;

import fr.wedoogift.backend.models.entities.Company;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.time.LocalDate;

@Entity
@DiscriminatorValue("gift")
public class Gift
        extends Deposit {

    public Gift() {
    }

    public Gift(Company issuerCompany, Integer amount, LocalDate date,
                LocalDate expiredDate) {
        super(issuerCompany, amount, date, expiredDate, "gift");
    }

    @Override
    public void calculateExpiredDate() {
        super.setExpiredDate(super.getDate()
                                  .toLocalDate()
                                  .plusYears(1)
                                  .plusDays(1));
    }
}
