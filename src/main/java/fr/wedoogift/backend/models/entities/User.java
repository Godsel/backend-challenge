package fr.wedoogift.backend.models.entities;

import fr.wedoogift.backend.models.UserAccountInterface;
import fr.wedoogift.backend.models.entities.deposit.Deposit;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Entity
@Table(name = "users")
public class User
        implements UserAccountInterface {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @OneToMany(mappedBy = "user",
               fetch = FetchType.EAGER,
               cascade = CascadeType.ALL)
    private List<Deposit> depositList;

    public User() {
    }

    public User(String userName) {
        this.name = userName;
        depositList = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Deposit> getDepositList() {
        return depositList;
    }

    public void setDepositList(List<Deposit> depositList) {
        this.depositList = depositList;
    }

    public void deposit(Deposit deposit) {
        depositList.add(deposit);
    }

    public Map<String, Integer> getBalanceByDepositType() {
        return depositList.stream()
                          .filter(Deposit::isValid)
                          .collect(Collectors.toMap(Deposit::getType,
                                                    Deposit::getAmount,
                                                    Integer::sum));

    }
}
