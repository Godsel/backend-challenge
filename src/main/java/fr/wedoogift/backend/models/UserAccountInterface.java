package fr.wedoogift.backend.models;

import fr.wedoogift.backend.models.entities.deposit.Deposit;

import java.util.Map;

public interface UserAccountInterface {
    void deposit(Deposit deposit);
    Map<String, Integer> getBalanceByDepositType();
}
