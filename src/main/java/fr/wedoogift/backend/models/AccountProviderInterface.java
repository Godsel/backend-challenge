package fr.wedoogift.backend.models;

import fr.wedoogift.backend.exceptions.BalanceException;
import fr.wedoogift.backend.models.entities.User;

public interface AccountProviderInterface {
    void provideDeposit(User user, Integer amount, String depositType)
            throws
            BalanceException;
}
