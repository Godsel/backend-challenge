package fr.wedoogift.backend.repositories;

import fr.wedoogift.backend.models.entities.deposit.Deposit;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepositRepository extends CrudRepository<Deposit, Long> {
}
