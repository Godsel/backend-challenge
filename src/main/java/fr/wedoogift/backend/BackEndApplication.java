package fr.wedoogift.backend;

import org.h2.tools.Server;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.sql.SQLException;

@SpringBootApplication
public class BackEndApplication {
    public static void main(String[] args) {
        SpringApplication.run(BackEndApplication.class, args);
    }

    @Bean(initMethod = "start", destroyMethod = "stop")
    public Server h2Server() throws
                             SQLException {
        return Server.createTcpServer("-tcp", "-webAllowOthers", "-tcpAllowOthers", "-tcpPort",
                                      "8081");
    }

}
