package fr.wedoogift.backend.config;

import fr.wedoogift.backend.exceptions.BalanceException;
import fr.wedoogift.backend.exceptions.UnexistantIdException;
import fr.wedoogift.backend.exceptions.UnexpectedDepositType;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ControllerExceptionHandler {
    @ExceptionHandler(UnexistantIdException.class)
    public ResponseEntity<String> handleNotFoundException(UnexistantIdException unexistantIdException) {
        return new ResponseEntity<>(unexistantIdException.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({BalanceException.class, UnexpectedDepositType.class})
    public ResponseEntity<String> handleBalanceException(RuntimeException runtimeException) {
        return new ResponseEntity<>(runtimeException.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
