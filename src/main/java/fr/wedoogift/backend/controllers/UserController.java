package fr.wedoogift.backend.controllers;

import fr.wedoogift.backend.models.dto.BalanceDto;
import fr.wedoogift.backend.models.dto.DepositCreationDto;
import fr.wedoogift.backend.models.dto.DepositDto;
import fr.wedoogift.backend.services.BasedUserServiceImpl;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE, consumes =
        MediaType.APPLICATION_JSON_VALUE
)
public class UserController {

    private final BasedUserServiceImpl basedUserService;

    public UserController(BasedUserServiceImpl basedUserService) {
        this.basedUserService = basedUserService;
    }

    @PostMapping("/{id}/deposits")
    @ResponseBody
    public ResponseEntity<List<DepositDto>> postDeposit(
            @PathVariable (name = "id")
                    Long userId,
            @RequestBody
            @Valid DepositCreationDto depositCreationDto) {
        return ResponseEntity.ok(basedUserService.postDeposit(userId, depositCreationDto));
    }

    @GetMapping("/{id}/balance")
    @ResponseBody
    public ResponseEntity<List<BalanceDto>> getBalancesByUserId(
            @PathVariable (name = "id")
                    Long userId) {
        return ResponseEntity.ok(basedUserService.getBalancesByUserId(userId));
    }
}
