insert into companies(ID, balance, name) values (1, 100, 'Wedoogift');
insert into companies(ID, balance, name) values (2 , 100, 'Fnac');

insert into users(id, name) values (1, 'John');
insert into users(id, name) values (2, 'Jessica');

insert into deposits(id, type, amount, date, expired_date, company_id, user_id) values (1, 'meal', 200, '2021-01-01', '2022-03-01', 2, 1);
insert into deposits(id, type, amount, date, expired_date, company_id, user_id) values (2, 'meal', 200, '2022-01-01', '2023-03-01', 2, 1);
insert into deposits(id, type, amount, date, expired_date, company_id, user_id) values (3, 'gift', 50, '2022-01-01', '2023-01-02', 2, 1);

insert into deposits(id, type, amount, date, expired_date, company_id, user_id) values (4, 'meal', 100, '2022-01-01', '2023-03-01', 1, 2);
insert into deposits(id, type, amount, date, expired_date, company_id, user_id) values (5, 'gift', 100, '2022-01-01', '2023-01-02', 1, 2);