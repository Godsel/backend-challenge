package controllers;

import fr.wedoogift.backend.controllers.UserController;
import fr.wedoogift.backend.models.dto.CompanyDto;
import fr.wedoogift.backend.models.dto.DepositCreationDto;
import fr.wedoogift.backend.models.dto.DepositDto;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.jdbc.Sql;
import utils.SpringTest;

import java.util.Arrays;
import java.util.List;

@SpringTest
@Sql(scripts = {"classpath:clean.sql", "classpath:init.sql"})
@Sql(scripts = {"classpath:clean.sql"},
     executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class UserControllerSpringTest {
    @Autowired
    UserController userController;
    @Autowired
    TestRestTemplate testRestTemplate;
    String basedUrl = "http://localhost:";
    @LocalServerPort
    private int localServerPort;

    @Test
    void should_return_404_code_when_provided_user_id_do_not_exist() {
        // Given
        Long userId = 5L;
        String url = basedUrl + localServerPort + "/users" + userId + "/balance";

        // When
        ResponseEntity<String> responseEntity = testRestTemplate.exchange(url,
                                                                          HttpMethod.GET,
                                                                          null,
                                                                          String.class);

        Assertions.assertThat(responseEntity.getStatusCode())
                  .isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void should_return_404_code_when_provided_company_id_does_not_exist() {
        // Given
        Long userId = 1L;
        String url = basedUrl + localServerPort + "/users" + userId + "/deposits";
        DepositCreationDto depositCreationDto = new DepositCreationDto(new CompanyDto(5L,
                                                                                      "Wedoogift"),
                                                                       Arrays.asList(new DepositDto(
                                                                                             null,
                                                                                             "meal",
                                                                                             80,
                                                                                             null,
                                                                                             null),
                                                                                     new DepositDto(
                                                                                             null,
                                                                                             "gift",
                                                                                             10,
                                                                                             null,
                                                                                             null)));

        HttpEntity<DepositCreationDto> requestEntity = new HttpEntity<>(depositCreationDto,
                                                                        getHttpHeader());


        // When
        ResponseEntity<String> responseEntity = testRestTemplate.exchange(url,
                                                                          HttpMethod.POST,
                                                                          requestEntity,
                                                                          String.class);

        Assertions.assertThat(responseEntity.getStatusCode())
                  .isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void should_return_400_code_when_provided_deposit_type_is_not_handled() {
        // Given
        Long userId = 1L;
        String url = basedUrl + localServerPort + "/users/" + userId + "/deposits";
        DepositCreationDto depositCreationDto = new DepositCreationDto(new CompanyDto(1L,
                                                                                      "Wedoogift"),
                                                                       Arrays.asList(new DepositDto(
                                                                               null,
                                                                               "vod",
                                                                               80,
                                                                               null,
                                                                               null)));

        HttpEntity<DepositCreationDto> requestEntity = new HttpEntity<>(depositCreationDto,
                                                                        getHttpHeader());


        // When
        ResponseEntity<String> responseEntity = testRestTemplate.exchange(url,
                                                                          HttpMethod.POST,
                                                                          requestEntity,
                                                                          String.class);

        Assertions.assertThat(responseEntity.getStatusCode())
                  .isEqualTo(HttpStatus.BAD_REQUEST);

        Assertions.assertThat(responseEntity.getBody())
                  .isEqualTo("This deposit type is not " +
                             "supported.");
    }

    @Test
    void should_return_400_code_when_provided_deposit_amount_is_too_high() {
        // Given
        Long userId = 1L;
        String url = basedUrl + localServerPort + "/users/" + userId + "/deposits";
        DepositCreationDto depositCreationDto = new DepositCreationDto(new CompanyDto(1L,
                                                                                      "Wedoogift"),
                                                                       Arrays.asList(new DepositDto(
                                                                               null,
                                                                               "gift",
                                                                               200,
                                                                               null,
                                                                               null)));

        HttpEntity<DepositCreationDto> requestEntity = new HttpEntity<>(depositCreationDto,
                                                                        getHttpHeader());


        // When
        ResponseEntity<String> responseEntity = testRestTemplate.exchange(url,
                                                                          HttpMethod.POST,
                                                                          requestEntity,
                                                                          String.class);

        Assertions.assertThat(responseEntity.getStatusCode())
                  .isEqualTo(HttpStatus.BAD_REQUEST);

        Assertions.assertThat(responseEntity.getBody())
                  .isEqualTo("Company's balance is too low to make this deposit");
    }

    // Record class seems to not be deserialized properly with TestRestTemplate
    // rg.springframework.http.converter.HttpMessageNotReadableException:
    /*@Test
    void should_return_expected_balance_for_user_John() {
        // Given
        Long userId = 1L;
        List<BalanceDto> expectedResult = Arrays.asList(new BalanceDto(50, "gift"),
                                                        new BalanceDto(200, "meal"));

        String url = basedUrl + localServerPort + "/users" + userId + "/balance";

        // When
        ResponseEntity<BalanceDto[]> responseEntity = testRestTemplate.getForEntity(url,
                                                                                    BalanceDto[].class);

        BalanceDto[] result = responseEntity.getBody();

        // Then
        Assertions.assertThat(Arrays.asList(result))
                  .usingRecursiveFieldByFieldElementComparator()
                  .isEqualTo(expectedResult);
    }*/

    private HttpHeaders getHttpHeader() {
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        requestHeaders.setAccept(List.of(MediaType.APPLICATION_JSON));
        return requestHeaders;
    }
}
