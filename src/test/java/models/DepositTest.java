package models;

import fr.wedoogift.backend.models.entities.Company;
import fr.wedoogift.backend.models.entities.deposit.Gift;
import fr.wedoogift.backend.models.entities.deposit.Meal;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.LocalDate;

class DepositTest {

    @Test
    void should_return_false_when_gift_deposit_is_older_than_366_days() {
        // Given
        Company company = new Company("Fnac", 100);
        LocalDate depositDate = LocalDate.now()
                                         .minusYears(1).minusDays(1);
        Gift giftDeposit = new Gift(company, 0, depositDate, null);

        // When
        giftDeposit.calculateExpiredDate();
        Boolean result = giftDeposit.isValid();

        // Then
        Assertions.assertThat(result)
                  .isFalse();
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 10, 50, 365})
    void should_return_true_when_gift_deposit_is_still_in_its_validity_time(Integer days) {
        // Given
        Company company = new Company("Fnac", 100);
        LocalDate depositDate = LocalDate.now()
                                         .minusDays(days);
        Gift giftDeposit = new Gift(company, 0, depositDate, null);

        // When
        giftDeposit.calculateExpiredDate();
        Boolean result = giftDeposit.isValid();

        // Then
        Assertions.assertThat(result)
                  .isTrue();
    }

    @Test
    void should_return_false_when_current_date_goes_beyond_february_and_follows_next_year_of_meal_deposit_date() {
        // Given
        Company company = new Company("Fnac", 100);
        LocalDate depositDate = getDepositDateForMealDeposit();
        Meal mealDeposit = new Meal(company, 0, depositDate, null);

        // When
        mealDeposit.calculateExpiredDate();
        Boolean result = mealDeposit.isValid();

        // Then
        Assertions.assertThat(result)
                  .isFalse();
    }

    @Test
    void should_return_true_when_meal_deposit_is_in_the_same_year_than_current_date() {
        // Given
        Company company = new Company("Fnac", 100);
        LocalDate depositDate = LocalDate.now().withDayOfYear(1);
        Meal mealDeposit = new Meal(company, 0, depositDate, null);

        // When
        mealDeposit.calculateExpiredDate();
        Boolean result = mealDeposit.isValid();

        // Then
        Assertions.assertThat(result)
                  .isTrue();
    }

    private LocalDate getDepositDateForMealDeposit() {
        if (LocalDate.now()
                     .getMonthValue() < 3) {
            return LocalDate.now()
                            .minusYears(2);
        } else {
            return LocalDate.now()
                            .minusYears(1);
        }
    }
}
