package models;

import fr.wedoogift.backend.exceptions.BalanceException;
import fr.wedoogift.backend.models.entities.Company;
import fr.wedoogift.backend.models.entities.User;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import utils.UnitTest;

@UnitTest
class CompanyTest {

    @Test
    void should_distribute_gift_deposits() {
        // Given
        Company company = new Company("Tesla", 100);
        Integer giftAmount = 100;

        User user = new User("John");

        // Then
        Assertions.assertThatCode(() -> company.provideDeposit(user, giftAmount, "gift"))
                  .doesNotThrowAnyException();
        Assertions.assertThat(user.getBalanceByDepositType())
                  .hasSizeGreaterThan(0);

    }

    @Test
    void should_distribute_meal_deposits() {
        // Given
        Company company = new Company("Tesla", 100);
        Integer giftAmount = 100;

        User user = new User("John");

        // Then
        Assertions.assertThatCode(() -> company.provideDeposit(user, giftAmount, "meal"))
                  .doesNotThrowAnyException();
        Assertions.assertThat(user.getBalanceByDepositType())
                  .hasSizeGreaterThan(0);

    }

    @Test
    void should_raise_balance_exception_when_company_balance_is_too_low() {
        // Given
        Company company = new Company("Tesla", 100);
        Integer giftAmount = 200;

        User user = new User("John");

        // Then
        Assertions.assertThatThrownBy(() -> company.provideDeposit(user, giftAmount, "gift"))
                  .isInstanceOf(BalanceException.class)
                  .hasMessage("Company's balance is too low to make this deposit");
    }


}
