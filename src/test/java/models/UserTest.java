package models;

import fr.wedoogift.backend.models.entities.Company;
import fr.wedoogift.backend.models.entities.User;
import fr.wedoogift.backend.models.entities.deposit.Deposit;
import fr.wedoogift.backend.models.entities.deposit.Gift;
import fr.wedoogift.backend.models.entities.deposit.Meal;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import utils.UnitTest;

import java.time.LocalDate;
import java.util.Map;

@UnitTest
class UserTest {

    @Test
    void should_put_deposit_of_100() {
        // Given
        User user = new User("John");
        Company company = new Company("Fnac", 100);
        Deposit deposit = new Meal(company, 100, LocalDate.now(), LocalDate.MAX);
        deposit.setType("meal");

        // When
        user.deposit(deposit);
        Map<String, Integer> balanceByDepositType = user.getBalanceByDepositType();

        // Then
        Assertions.assertThat(balanceByDepositType)
                  .isNotEmpty()
                  .containsEntry("meal", 100);
    }

    @Test
    void should_put_deposits_of_100_and_200() {
        // Given
        User user = new User("John");
        Company company = new Company("Fnac", 100);
        Deposit firstDeposit = new Gift(company, 100, LocalDate.now(), LocalDate.MAX);
        Deposit secondDeposit = new Gift(company, 200, LocalDate.now(), LocalDate.MAX);
        firstDeposit.setType("gift");
        secondDeposit.setType("gift");

        // When
        user.deposit(firstDeposit);
        user.deposit(secondDeposit);
        Map<String, Integer> balanceByDepositType = user.getBalanceByDepositType();

        // Then
        Assertions.assertThat(balanceByDepositType)
                  .isNotEmpty()
                  .containsEntry("gift", 300);

    }

    @Test
    void should_return_empty_balance_when_deposit_is_outdated() {
        // Given
        User user = new User("John");
        Company company = new Company("Fnac", 100);

        Deposit firstDeposit = new Gift(company, 100,
                                        LocalDate.now(),
                                        LocalDate.now()
                                                 .minusDays(1));
        Deposit secondDeposit = new Meal(company,
                                         200,
                                         LocalDate.now(),
                                         LocalDate.now()
                                                  .minusDays(1));

        // When
        user.deposit(firstDeposit);
        user.deposit(secondDeposit);

        // Then
        Assertions.assertThat(user.getBalanceByDepositType())
                  .isEmpty();
    }

}
