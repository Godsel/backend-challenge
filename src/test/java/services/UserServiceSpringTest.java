package services;

import fr.wedoogift.backend.exceptions.BalanceException;
import fr.wedoogift.backend.exceptions.UnexistantIdException;
import fr.wedoogift.backend.exceptions.UnexpectedDepositType;
import fr.wedoogift.backend.models.dto.BalanceDto;
import fr.wedoogift.backend.models.dto.CompanyDto;
import fr.wedoogift.backend.models.dto.DepositCreationDto;
import fr.wedoogift.backend.models.dto.DepositDto;
import fr.wedoogift.backend.models.entities.deposit.Deposit;
import fr.wedoogift.backend.models.entities.deposit.Meal;
import fr.wedoogift.backend.services.BasedUserServiceImpl;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import utils.SpringTest;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@SpringTest
@Sql(scripts = {"classpath:clean.sql", "classpath:init.sql"})
@Sql(scripts = {"classpath:clean.sql"},
     executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class UserServiceSpringTest {
    @Autowired
    BasedUserServiceImpl userService;

    @Test
    void should_throw_unexistant_id_exception_when_posting_deposits_with_unexistant_user_id() {
        // Given
        Long userId = 9999999L;
        DepositCreationDto depositCreationDto = new DepositCreationDto(new CompanyDto(1L,
                                                                                      "Wedoogift"),
                                                                       Collections.singletonList(new DepositDto(
                                                                               null,
                                                                               "gift",
                                                                               100,
                                                                               null,
                                                                               null)));

        // When

        Assertions.assertThatThrownBy(() -> userService.postDeposit(userId,
                                                                    depositCreationDto))
                  .isInstanceOf(
                          UnexistantIdException.class);
    }

    @Test
    void should_throw_unexistant_id_exception_when_posting_deposits_with_unexistant_company_id() {
        // Given
        Long userId = 1L;
        DepositCreationDto depositCreationDto = new DepositCreationDto(new CompanyDto(99L, "Fake " +
                                                                                           "Company"),
                                                                       Collections.singletonList(new DepositDto(
                                                                               null,
                                                                               "meal",
                                                                               100,
                                                                               null,
                                                                               null)));

        // When

        Assertions.assertThatThrownBy(() -> userService.postDeposit(userId,
                                                                    depositCreationDto))
                  .isInstanceOf(
                          UnexistantIdException.class);
    }

    @Test
    void should_return_expected_deposits_for_user_Jessica() {
        // Given
        Long userId = 2L;
        DepositCreationDto depositCreationDto = new DepositCreationDto(new CompanyDto(1L,
                                                                                      "Wedoogift"),
                                                                       Arrays.asList(new DepositDto(
                                                                                             null,
                                                                                             "meal",
                                                                                             80,
                                                                                             null,
                                                                                             null),
                                                                                     new DepositDto(
                                                                                             null,
                                                                                             "gift",
                                                                                             10,
                                                                                             null,
                                                                                             null)));

        Deposit mealDeposit = new Meal(null, null, LocalDate.now(), null);
        mealDeposit.calculateExpiredDate();
        List<DepositDto> expected = Arrays.asList(new DepositDto(
                                                          6L,
                                                          "meal",
                                                          80,
                                                          LocalDate.now()
                                                                   .atStartOfDay(),
                                                          mealDeposit.getExpiredDate()),
                                                  new DepositDto(
                                                          7L,
                                                          "gift",
                                                          10,
                                                          LocalDate.now()
                                                                   .atStartOfDay(),
                                                          LocalDate.now()
                                                                   .plusYears(1)
                                                                   .plusDays(1)));

        // When
        List<DepositDto> result = userService.postDeposit(userId, depositCreationDto);

        // Then
        Assertions.assertThat(result)
                  .usingRecursiveFieldByFieldElementComparator()
                  .isEqualTo(expected);


    }

    @Test
    void should_throw_unexpected_deposit_type_when_giving_unknown_type_in_input() {
        // Given
        Long userId = 2L;
        DepositCreationDto depositCreationDto = new DepositCreationDto(new CompanyDto(1L,
                                                                                      "Wedoogift"),
                                                                       List.of(new DepositDto(
                                                                               null,
                                                                               "movie",
                                                                               80,
                                                                               null,
                                                                               null)));

        // When
        Assertions.assertThatCode(() ->
                                          userService.postDeposit(userId, depositCreationDto))
                  .isInstanceOf(UnexpectedDepositType.class);


    }

    @Test
    void should_throw_balance_exception_type_when_giving_unknown_type_in_input() {
        // Given
        Long userId = 2L;
        DepositCreationDto depositCreationDto = new DepositCreationDto(new CompanyDto(1L,
                                                                                      "Wedoogift"),
                                                                       List.of(new DepositDto(
                                                                               null,
                                                                               "gift",
                                                                               300,
                                                                               null,
                                                                               null)));

        // When
        Assertions.assertThatCode(() ->
                                          userService.postDeposit(userId, depositCreationDto))
                  .isInstanceOf(BalanceException.class);
    }

    @Test
    void retrieving_balance_with_incorect_user_id_should_throw_unexistant_id_exception() {
        // Given
        Long userId = 0L;

        // When
        Assertions.assertThatThrownBy(() -> userService.getBalancesByUserId(userId))
                  .isInstanceOf(
                          UnexistantIdException.class);
    }

    @Test
    void should_return_expected_balance_for_user_John() {
        // Given
        Long userId = 1L;
        List<BalanceDto> expectedResult = Arrays.asList(new BalanceDto(50, "gift"),
                                                        new BalanceDto(200, "meal"));

        // When
        List<BalanceDto> result = userService.getBalancesByUserId(userId);

        // Then
        Assertions.assertThat(result)
                  .usingRecursiveFieldByFieldElementComparator()
                  .isEqualTo(expectedResult);
    }


}
