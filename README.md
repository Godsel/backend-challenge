Version java 17


Lancer le programme

1/ mvn clean install (lance les tests)
2/ java -jar target/backend-1.0-SNAPSHOT.jar

2 endpoints : 

- POST http://localhost:8080/users/1/deposits

exemple du body : 

	{
		"company" : {
			"id" : "1",
			"name": "Example"
		},
		
		"deposits" : [
			{
				"type": "meal",
				"amount": "10"
			}
		]
	}

- GET http://localhost:8080/users/1/balance



Notes/Remarques : 

- springboot avec un H2 en mémoire
- Certains choix sont discutables, notamment les endpoint et la responsabilité de l'ajout des dépôts via l'user (on pourrait plutôt partir sur la companie..)
- La date des dépôts ajoutés correspondra au moment où on les ajoute (la date du jour donc).. Je me suis posé la question de la faisabilité si je donnais la possibilité d'ajouter une date spécifique (l'impact sur la balance du compte de l'entreprise à un instant T..)
- J'aurai pu créer une table supplémentaire recensant les différents types de dépôts, j'ai fait le choix de gérer les types directement par chaîne de caractère plutôt que par id
